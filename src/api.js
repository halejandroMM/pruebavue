
import axios from 'axios'

/*const client = axios.create({
  baseURL: 'http://localhost:8081/',
  json: true
})*/
const api = 'http://localhost:8081';
export default {
 
 async getDataDomain (domainName) {
    return await axios.get(`${api}/domain/${domainName}`)
  },

 async getSearchHistory () {
    return await axios.get(`${api}/all/`)
  },
}