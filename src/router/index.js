import Vue from "vue";
import VueRouter from "vue-router";
import CheckDomain from "@/views/CheckDomain.vue"
import SearchHistory from "@/views/SearchHistory.vue"
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "check",
    component: CheckDomain
  },
  {
    path: "/history",
    name: "history",
    component: SearchHistory
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;